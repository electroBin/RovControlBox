;Convert analog signal from joystick to PWM values to be sent to 6 thrusters
;via UART (Main file for Control Box)

    list	p=16f1937	   ;list directive to define processor
    #include	<p16f1937.inc>	   ;processor specific variable definitions
    #include    <mainConfig.inc>
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
   
;**********************************************************************
.ResetVector code	0x000	
    pagesel		start	; processor reset vector
    goto		start	; go to beginning of program
INT_VECTOR:
.Interupt code		0x004		 ;interrupt vector location
INTERRUPT:
    banksel	w_copy
    movwf       w_copy           ;save off current W register contents
    movfw        STATUS         ;move status register into W register
    movwf       status_copy      ;save off contents of STATUS register
    movf        PCLATH,W
    movwf       pclath_copy
    
;PORTB interrupt on change 
    ;Determine which PORTB pin triggered interrupt
    banksel	IOCBF
    btfsc	IOCBF, 0    ;PORTB, 0 = joystick pb switch
    goto	JoystickPBswitch
    goto	GainRotswitch	;PORTB, 3 4 or 5 for rotary switch (gain selection)
    goto	isrEnd
;*******************Interrupt is due to gain adjustment*************************
GainRotswitch
    pagesel	GainAdjust
    call	GainAdjust
    pagesel$
    goto	isrEnd
;********************Interrupt is due to joystick push-button switch************
JoystickPBswitch
    pagesel	motors
    call	motors
    pagesel$
    goto	isrEnd

;Restore interrupt context-saving variables
isrEnd	
    banksel	pclath_copy
    movfw	pclath_copy
    movwf	PCLATH
    movf	status_copy,w   ;retrieve copy of STATUS register
    movwf	STATUS          ;restore pre-isr STATUS register contents
    swapf	w_copy,f
    swapf	w_copy,w
    retfie

delayMillis
    movwf	userMillis	;user defined number of milliseconds
startDly
    banksel	TMR0
    clrf	TMR0
waitTmr0
    movlw	.31		;31 * 32uS = 1mS
    banksel	TMR0
    subwf	TMR0, w
    btfss	STATUS, C	;C=0 if neg result 
    goto	waitTmr0
    decfsz	userMillis, f	;reached user defined milliseconds yet?
    goto	startDly
    
    retlw	0

Delay16Us
    clrf	    dly16Ctr	;zero out delay counter
begin
    nop			;1 uS (4Mhz clock/4 = 1uS per instruction
    banksel	    dly16Ctr
    incf	    dly16Ctr, f
    movlw	    .16		
    xorwf	    dly16Ctr, w	 ;16 uS passed?
    btfss	    STATUS, Z
    goto	    begin	;no so keep looping
    retlw	    0    
   
.main    code	
start:
    
    
    pagesel	peripheralInit
    call	peripheralInit	    ;initialize peripherals
    pagesel$
    
    pagesel	ESCinitializing
    call	ESCinitializing
    pagesel$

    banksel	receiveData
    clrf	receiveData
    
    ;Wait until ROV is ready
Holdup    
    pagesel	Receive
    call	Receive
    pagesel$
    movlw	.245
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	Holdup
    ;Send a number other than 245 to tell ROV to proceed
    movlw	.247
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
    pagesel	displayHeaders  ;This was added after removing the ESC ready stuff
    call	displayHeaders
    pagesel$   
    
    ;Determine initial gain setting:
    ;Once high pin is detected, remove it from IOCBP so it doesn't continuously 
    ;trigger an interrupt
    banksel	PORTB
    btfss	PORTB, 3	;=25% gain
    goto	initTest50
    pagesel	Disp25percent
    call	Disp25percent
    pagesel$
    banksel	IOCBP
    bcf		IOCBP, 3	;Set PORTB, 3 for IOC on falling edge
    banksel	IOCBN
    bsf		IOCBN, 3
    goto	DoneInitGain
initTest50
    banksel	PORTB
    btfss	PORTB, 4	;=50% gain
    goto	initTest75
    pagesel	Disp50percent
    call	Disp50percent
    pagesel$
    banksel	IOCBP
    bcf		IOCBP, 4	;Set PORTB, 4 for IOC on falling edge
    banksel	IOCBN
    bsf		IOCBN, 4
    goto	DoneInitGain
initTest75
    banksel	PORTB
    btfss	PORTB, 5	;=75% gain
    goto	initTest100
    pagesel	Disp75percent
    call	Disp75percent
    pagesel$
    banksel	IOCBP
    bcf		IOCBP, 5	;Set PORTB, 5 for IOC on falling edge
    banksel	IOCBN
    bsf		IOCBN, 5
    goto	DoneInitGain
initTest100
    pagesel	Disp100percent
    call	Disp100percent
    pagesel$
    
DoneInitGain
    
    bsf		INTCON, IOCIE	;Ennable IOC for PORTB Now that ESCs are initialized    
    banksel	IOCBP
    bsf		IOCBP, IOCBP0   ;Enable joystick PB switch
    
    banksel	PIE1
    clrf	PIE1	;Disable UART receive interrupts
    
    ;Turn on the "ESC READY" Light
    banksel	PORTC
    bsf		PORTC, 1
    
mainLoop
    ;Check to see if we need to update LCD for batery voltages;
    ;banksel	flags
    ;btfss	flags, 0
    ;goto	StartADC
    ;pagesel	VoltageUpdate
    ;call	VoltageUpdate
    ;pagesel$
    
StartADC    
;1)********************Check camera joystick (AN4)******************************
    ;Set AN4 as analog input for AD conversion and start AD conversion
    CameraServo
    movlw	b'00010001'
		 ;-00100	CHS<0:4> (bits 2-6) = 00100 = pin AN4)/PORTA, 5 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel ctr
    clrf    ctr
    clrf    servoPosition
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitCameraAdc
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitCameraAdc
    
    ;Scale ADC result:
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH4
    movwf	ADRESH4	    ;get a copy of ADRESH
    
    movlw	.56	    ;start with servo in extreme-most position
    banksel	servoPosition
    movwf	servoPosition
startAdding
    ;First check if servoPosition is with 3 of max value yet (131)
    movlw	.130
    banksel	servoPosition
    subwf	servoPosition, w
    btfsc	STATUS, C	;C=0=neg number
    goto	servoDone
    incf	servoPosition, f    ;increment servo by one
    movlw	.2
    addwf	ctr, f
    movfw	ctr
    subwf	ADRESH4, w
    btfsc	STATUS, C	    ;reached value of ADC yet (C=0=neg number)
    goto	startAdding
servoDone
    
;3)*****************Check ROV Lights Potentiometer******************************
    ;Set AN2 as analog input for AD conversion and start AD conversion
    movlw	b'00001001'
		 ;-00010	CHS<0:4> (bits 2-6) = 00010 = pin AN2)/PORTA, 2 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitLightsAdc
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitLightsAdc
    
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH4
    movwf	ADRESH4
    
    ;Scale ADC result
    movlw	.33
    banksel	lightsPWM
    movwf	lightsPWM	    ;Start with lights off
    banksel ctr
    clrf    ctr			    
getLights
    ;First check if lights PWM is at max value yet (60)
    movlw	.60
    banksel	lightsPWM
    xorwf	lightsPWM, w
    btfsc	STATUS, Z
    goto	lightsDone	    ;Lights PWM value is maxed out so exit loop
    
    incf	lightsPWM, f
    movlw	.9
    addwf	ctr, f
    movfw	ctr
    banksel	ADRESH4
    subwf	ADRESH4, w
    btfsc	STATUS, C	    ;reached value of ADC yet (C=0=neg number)
    goto	getLights
lightsDone    
    
;2)*************Check AN0 (FORWARD/REVERSE DIRECTION)***************************
    ;Set AN0 as analog input for AD conversion and start AD conversion
    movlw	b'00000001'
		; -00000--  CHS<0:4> (bits 2-6) = 00000 = pin AN0/PORTA, 0 as analog input
		; ------0-  stop AD conversion
		; -------1  Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    call	Delay16Us
    call	Delay16Us
    banksel	ADCON0
    bsf		ADCON0, GO
waitAdc3
    btfsc	ADCON0, NOT_DONE
    goto	waitAdc3
    
    banksel	ADRESH
    movfw	ADRESH
    movwf	ADRESH0
    
;3)**************Check AN1 (LEFT/RIGHT DIRECTION)*******************************
    ;Set AN1 as analog input for AD conversion and start AD conversion
    movlw	b'00000101'
		; -00000--  CHS<0:4> (bits 2-6) = 00001 = pin AN1/PORTA, 1 as analog input
		; ------0-  stop AD conversion
		; -------1  Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    call	Delay16Us
    call	Delay16Us
    banksel	ADCON0
    bsf		ADCON0, GO
waitAdc2
    btfsc	ADCON0, NOT_DONE
    goto	waitAdc2
    
    banksel	ADRESH
    movfw	ADRESH
    movwf	ADRESH1
;Check for joystick slop
    pagesel	checkSlop
    call	checkSlop
    pagesel$
    banksel	slopFlag
    btfsc	slopFlag, 0
    goto	RoboARM		;slop, so reloop
Displacement
    ;Check whether AN0 or AN1 value is further away from 127
    
;1) Get AN0 displacement from 127
    pagesel	getAn0Disp
    call	getAn0Disp
    pagesel$
    
;2) Get AN1 displacement from 127     
    pagesel	getAn1Disp
    call	getAn1Disp
    pagesel$

    ;subtract AN1disp from AN0disp to see which displacement is greater
    movfw	AN1disp
    subwf	AN0disp, w
    btfss	STATUS, C	;(C=0 is neg #)
    goto	leftRight	;AN1 (lt/rt) is greater
    goto	fwdRev		;ANO (fwd/rev) is greater
    
fwdRev 
    ;Determine whether we need to go forward or reverse
    ;ADRESH0 > 127 = forward******ADRESH0 <= 127 = reverse
    btfss	ADRESH0, 7
    goto	reverse
    
forward
    movlw	.0		;"forward" state
    banksel	state
    movwf	state
    ;Forward PWM (normal value) to thrusters 1 and 2 (top-left and top-right thrusters)
    movfw	ADRESH0
    movwf	ADRESHc
    pagesel	getMotorSpeed
    call	getMotorSpeed
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed
    movwf	forwardSpeed
    ;Reverse PWM (calculated value) to thrusters 3 and 4 (bottom left/bottom right thrusters)
    ;get displacement of ADRESH0 (already calculated above)
    movlw	.128		;128 instead of 127 to prevent overflow
    movwf	ADRESHc
    movfw	AN0disp
    subwf	ADRESHc, f	;and subtract displacement from 128 
    pagesel	getMotorSpeed
    call	getMotorSpeed	;to get a reverse PWM value and output it to the
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed	;reverse logic IC via P2A
    movwf	reverseSpeed
    goto        RoboARM
    
reverse
    movlw	.1		;"reverse" state
    banksel	state
    movwf	state
    ;Reverse PWM (normal value) to thrusters 1/2 (top-left and top-right thrusters)
    movfw	ADRESH0
    movwf	ADRESHc
    pagesel	getMotorSpeed
    call	getMotorSpeed
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed
    movwf	reverseSpeed
    ;Forward PWM (calculated value) to thrusters 3/4 (bottom-right and bottom-left thrusters)
    ;get displacement of ADRESH0 (already calculated above)
    movlw	.127		
    movwf	ADRESHc
    movfw	AN0disp
    addwf	ADRESHc, f	;and subtract displacement from 128 
    pagesel	getMotorSpeed
    call	getMotorSpeed	;to get a forward PWM value and output it to the
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed	;forward logic IC via P1A
    movwf	forwardSpeed
    goto	RoboARM
    
leftRight
    ;Determine whether we need to go left or right:
    ;(ADRESH1 > 127 = right********ADRESH1 <= 127 = left)
    btfss	ADRESH1, 7	;test MSB of ADRESH1 (1: > 127, 0: <= 127)
    goto	traverseLeft
    
traverseRight
    movlw	.2		;"traverse right" state
    banksel	state
    movwf	state
    ;Forward PWM (normal value) to thrusters 1 and 3 (top-left and bottom-left thrusters)
    movfw	ADRESH1		;send normal PWM value from ADC conversion
    movwf	ADRESHc		;to thrusters 1 and 4 via the forward logic
    pagesel	getMotorSpeed
    call	getMotorSpeed	;IC and through P1A
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed
    movwf	forwardSpeed
    ;Reverse PWM (calculated value) to thrusters 2 and 4 (top-right and bottom-right thrusters)
    ;get displacement of ADRESH1 (already calculated above)
    movlw	.128		;128 instead of 127 to prevent overflow
    movwf	ADRESHc
    movfw	AN1disp
    subwf	ADRESHc, f	;and subtract displacement from 128 
    pagesel	getMotorSpeed
    call	getMotorSpeed	;to get a reverse PWM value and output it to the
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed	;reverse logic IC via P2A
    movwf	reverseSpeed
    goto	RoboARM
    
traverseLeft
    movlw	.3		;"traverse left" state
    banksel	state
    movwf	state
    ;Reverse PWM (normal value) to thrusters 1 and 3 (top-left and bottom-left thrusters)
    movfw	ADRESH1
    movwf	ADRESHc
    pagesel	getMotorSpeed
    call	getMotorSpeed
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed
    movwf	reverseSpeed
    ;Forward PWM (calculated value) to thrusters 2 and 4 (top-right and bottom-right thrusters)
    ;get displacement of ADRESH1 (already calculated above)
    movlw	.127		
    movwf	ADRESHc
    movfw	AN1disp
    addwf	ADRESHc, f	;and subtract displacement from 128 
    pagesel	getMotorSpeed
    call	getMotorSpeed	;to get a forward PWM value and output it to the
    pagesel$
    banksel	positionSpeed
    movfw	positionSpeed	;forward logic IC via P1A
    movwf	forwardSpeed
    
    
RoboARM    
;*******************Calculate lead-screw position*******************************
    ;Set AN3 as analog input for AD conversion and start AD conversion
    movlw	b'00001101'
		 ;-00011	CHS<0:4> (bits 2-6) = 00011 = pin AN3/PORTA, 3 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitLeadScrew
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitLeadScrew
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH0
    movwf	ADRESH0
    
    ;Check for slop in lead screw joystick 
    ;Neutral ADC value range = 127-131
    movlw	.126
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	NoScrewSlop
    movlw	.133
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	NoScrewSlop
    ;Joystick is in neutral position
    movlw	screwStop
    banksel	leadScrewPWM
    movwf	leadScrewPWM
    goto	doneLeadScrew
    
NoScrewSlop
    ;Determine if we are moving forward or reverse:
    movlw	.129	    ;129=ADC neutral value
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C   ;C=0 for neg result
    goto	ScrewRev
    
ScrewFwd    ;We are extending arm outwards
    movlw	.95	    
    banksel	leadScrewPWM
    movwf	leadScrewPWM	;Start with minimum PWM forward speed
    movlw	.131
    movwf	ADCctr		;Start with minimum fwd ADC value
calcFwdScrew	;Start calculating our fwd screw speed
    movlw	.118		;Make sure we havent exceeded max PWM value 
    banksel	leadScrewPWM
    subwf	leadScrewPWM, w
    btfsc	STATUS, C	;C=0if neg result
    goto	doneLeadScrew	;We have hit a PWM value of 118 so exit loop
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	doneLeadScrew
    incf	leadScrewPWM, f	;Increment lead screw PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcFwdScrew	;Keep calculating
    
ScrewRev    ;We are bringing arm inwards
    movlw	.68	    
    banksel	leadScrewPWM
    movwf	leadScrewPWM	;Start with maximum PWM reverse speed
    movlw	.99
    movwf	ADCctr		;Start with maximum rev ADC value
calcRevScrew	;Start calculating our rev screw speed 
    movlw	.93		;Make sure we havent exceeded rev PWM value
    banksel	leadScrewPWM
    subwf	leadScrewPWM, w
    btfsc	STATUS, C	;C=0if neg result
    goto	doneLeadScrew	;We have hit a PWM value of 93 so exit loop
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	doneLeadScrew
    incf	leadScrewPWM, f	;Increment lead screw PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcRevScrew	;Keep calculating
    
doneLeadScrew	;Send lead screw value to controller
;*************Finsished calculating lead-screw position*************************
    
;*************Calculate gripper tilt position***********************************
GripTilt
;Set AN5 as analog input for AD conversion and start AD conversion
    movlw	b'00010101'
		 ;-00101	CHS<0:4> (bits 2-6) = 00101 = pin AN5/PORTE, 0 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitGripperTilt
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitGripperTilt
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH0
    movwf	ADRESH0
    
    ;Check for slop in gripper tilt joystick 
    ;Neutral ADC value range = 127-131
    movlw	.127
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	NoSlop
    movlw	.132
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneTilt	;Joystick is in the neutral position
NoSlop    
    ;	ADC range: 99-160
    ;	PWM range: 0.9ms-2ms (56-122)
    ;Determine if we are tilting up or down
    movlw	.129		;129 = neutral ADC position
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg resutl
    goto	TiltDown
TiltUp ;(Joystick pulled backwards-decrease voltage)
    ;1st check if gripTiltPWM is at minimum value yet (57)
    movlw	.57
    banksel	gripTiltPWM
    xorwf	gripTiltPWM, w
    btfsc	STATUS, Z
    goto	DoneTilt	;Gripper is already max tilted upwards so exit
    ;Scale the speed of upward tilt so it matches how for back the joystick is pressed
    ;1) Check for slow upward tilt speed (120<=ADC<=129)
    movlw	.120
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	SlowUpTilt
    ;2) Check for medium upward tilt speed (110<=ADC<=119)
    movlw	.110
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	MedUpTilt
    ;3) Last option is fast upward tilt speed (99<=ADC<=109)
FastUpTilt
    ;Check to see if we are within 5 from the min PWM value of 57 (value of 62)
    movlw	.62
    banksel	gripTiltPWM
    subwf	gripTiltPWM, w
    btfss	STATUS, C	;C=0 if neg result
    goto	SlowUpTilt	;Within 5 from a value of 57 so increment PWM by one
    movlw	.5		;Subtract 5 from PWM value
    subwf	gripTiltPWM, f
    goto	DoneTilt
MedUpTilt
    ;Check to see if we are within 3 from the min PWM value of 57 (value of 60)
    movlw	.60
    banksel	gripTiltPWM
    subwf	gripTiltPWM, w
    btfss	STATUS, C	;C=0 if neg result
    goto	SlowUpTilt	;Within 5 from a value of 57 so increment PWM by one
    movlw	.3		;Subtract 3 from PWM value
    subwf	gripTiltPWM, f
    goto	DoneTilt
    
SlowUpTilt
    ;Decrement the tilt PWM value by one
    banksel	gripTiltPWM
    decf	gripTiltPWM, f
    goto	DoneTilt
    
TiltDown ;(Joystick pushed forwards-increase voltage)
    ;1st check if gripTiltPWM is at max value yet (122)
    movlw	.122
    banksel	gripTiltPWM
    xorwf	gripTiltPWM, w
    btfsc	STATUS, Z
    goto	DoneTilt	;Gripper is already max tilted downwards so exit
    ;Scale the speed of downward tilt so it matches how far forward the joystick is pressed
    ;1) Check for slow downward tilt speed (130<=ADC<=139)
    movlw	.140
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	SlowTiltDown
    ;2) Check for medium tilt down speed (140<=ADC<=149)
    movlw	.150
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	MedTiltDown
    ;Last option if fast downward tilt speed (150<=ADC<=160)
FastTiltDown
    ;Check to see if we are within 5 from the max PWM value of 122 (value of 117)
    movlw	.118
    banksel	gripTiltPWM
    subwf	gripTiltPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	SlowTiltDown
    movlw	.5		;Add 5 to PWM value
    addwf	gripTiltPWM, f
    goto	DoneTilt
MedTiltDown
    ;Check to see if we are within 3 from the max PWM value of 122 (value of 119)
    movlw	.120
    banksel	gripTiltPWM
    subwf	gripTiltPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	SlowTiltDown
    movlw	.3		;Add 3 to PWM value
    addwf	gripTiltPWM, f
    goto	DoneTilt
SlowTiltDown    
    ;Increment the tilt PWM value by one
    banksel	gripTiltPWM
    incf	gripTiltPWM, f
DoneTilt    
    
;********************Finished calculating gripper tilt position*****************
				
;********************Calculate gripper claw position****************************
GripJaw    
    ;Set AN6 as analog input for AD conversion and start AD conversion
    movlw	b'00011001'
		 ;-00110	CHS<0:4> (bits 2-6) = 00110 = pin AN6/PORTE, 1 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitGripperJaw
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitGripperJaw
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH0
    movwf	ADRESH0
    
    ;Check for slop in gripper jaw joystick 
    ;Neutral ADC value range = 127-131
    movlw	.125
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	NoSlopJaw
    movlw	.127
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	GripperDone	;Joystick is in the neutral position
NoSlopJaw 
    ;	ADC range: 99-160
    ;	PWM range: 0.4ms-2.6ms (26-161)
    ;Determine if we are tilting up or down
    ;Determine if we are opening or closing jaw (max voltage=max closed)
    movlw	.129		;129 = neutral ADC position
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg resutl
    goto	CloseJaws
OpenJaws ;(Joystick pushed left-decrease voltage)
    ;1st check if gripJawPWM is at minimum value yet (26)
    movlw	.26
    banksel	gripJawPWM
    xorwf	gripJawPWM, w
    btfsc	STATUS, Z
    goto	GripperDone      ;Jaws are already max opened so exit
    ;Scale the speed of opening so it matches how far left the joystick is pressed
    ;1) Check for slow open speed (112<=ADC<=128)
    movlw	.112
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	SlowOpen
    ;2) Last option is open speed (99<=ADC<=109)
FastOpen
    ;Check to see if we are within 5 from the min PWM value of 26 (value of 31)
    movlw	.31
    banksel	gripJawPWM
    subwf	gripJawPWM, w
    btfss	STATUS, C	;C=0 if neg result
    goto	SlowOpen 	;Within 5 from a value of 26 so slow incrementation
    movlw	.5		;Subtract 10 from PWM value
    subwf	gripJawPWM, f
    goto	GripperDone
SlowOpen
    ;Decrement the jaw PWM value by one
    banksel	gripJawPWM
    decf	gripJawPWM, f
    goto	GripperDone
    
CloseJaws ;(Joystick pushed right-increase voltage)
    ;1st check if gripJawPWM is at max value yet (161)
    movlw	.161
    banksel	gripJawPWM
    xorwf	gripJawPWM, w
    btfsc	STATUS, Z
    goto	DoneTilt	;Gripper is already max closed so exit
    ;Scale the speed of closing so it matches how for right the joystick is pressed
    ;1) Check for slow closing speed (130<=ADC<=145)
    movlw	.146
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	SlowClose
    ;2) Last option is fast close speed (152<=ADC<=160)
FastClose
    ;Check to see if we are within 5 from the max PWM value of 161 (value of 156)
    movlw	.157
    banksel	gripJawPWM
    subwf	gripJawPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	SlowClose
    movlw	.5		;Add 5 to PWM value
    addwf	gripJawPWM, f
    goto	GripperDone
SlowClose
    ;Increment the tilt PWM value by one
    banksel	gripJawPWM
    incf	gripJawPWM, f
    
GripperDone
				
;Beeping sound for leak detector:
    banksel	PORTC
    btfss	PORTC, 0    ;LED for leak indicator is connected to this pin
    goto	UpdateThrust    ;No Leak
    movlw	.125
    banksel	CCPR1L
    xorwf	CCPR1L, f   ;Toggle PWM value
    
    
UpdateThrust
    ;Send Thruster data 
    pagesel	sendThrust
    call	sendThrust
    pagesel$	
    ;Determine if we need to get temperature data
    movlw	tempInterval
    banksel	tempIntCtr
    xorwf	tempIntCtr, w
    btfss	STATUS, Z
    goto	CheckBattery
    pagesel	MS5837
    call	MS5837
    pagesel$
CheckBattery    
    ;Determine if we need to read the battery voltage levels
    movlw	battInterval
    banksel	battIntCtr
    xorwf	battIntCtr, w
    btfss	STATUS, Z
    goto	mainLoop
    pagesel	BatteryVoltage
    call	BatteryVoltage
    pagesel$
    	
    goto	mainLoop
   
    END                       







































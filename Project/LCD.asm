    ;LCD Routines


    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    extern  delayMillis
    extern  stringIndex1
    extern  Temperature
    extern  LCDdigit
    extern  cell1
    extern  cell2
    extern  cell3
    extern  cell4
    extern  cellShadow
    extern  flags
    extern  div8
    extern  Q_dividend
    extern  divisor
    
    global  LCDInit
    global  sendData
    global  sendCommand
    global  displayHeaders
    global  dispTemp
    global  Disp25percent
    global  Disp50percent
    global  Disp75percent
    global  Disp100percent
    global  PrintLeak
    global  VoltageUpdate
    global  ESCinitializing
    global  PrintReset
    
;Set command mode
.lcd code
RS0
    banksel	PORTB
    bcf		PORTB, 2	;PORTB, 2/RS=0
    retlw	0
;Set character mode
RS1
    banksel	PORTB
    bsf		PORTB, 2	;PORTB, 2/RS=1
    retlw	0
;Pulse E (enable)
ePulse
    banksel	PORTB
    bsf		PORTB, 1		;take E line higH
    nop	
    nop					;hold for 3 clock cycles
    nop
    bcf		PORTB, 1		;take E line low
    retlw	0
	
;Send a command to LCD (command is already in work register)
sendCommand
    banksel	PORTD
    movwf	PORTD	    ;send command to PORTB
    call	RS0	    ;Enter command mode
    call	ePulse	    ;pulse E line
    movlw	.2
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    retlw	0
	
;Send character data to LCD (data is already in work register)
sendData
    banksel	PORTD
    movwf	PORTD	    ;send character to PORTB
    call	RS1	    ;Enter character mode
    call	ePulse	    ;pulse E line
    movlw	.2
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    retlw	0	


;*************************Display "LEAK" warning on LCD*************************
PrintLeak
    banksel	stringIndex1	    ;Clear out string index counter
    clrf	stringIndex1
    ;Set display address to beginning of line
    movlw	0xD4
    call	sendCommand
dispLk
    call	getString3	;display "(LEAK !!)"
    ;finish if null
    iorlw	0
    btfsc	STATUS, Z	;end of string?
    goto	doneLk  	;if end of string quit looping
    ;Write character to display:
    call	sendData	;write char
    movlw	.1
    pagesel	delayMillis
    call	delayMillis	;delay
    pagesel$
    banksel	stringIndex1
    incf	stringIndex1, f	;increment string index to point to next char
    goto	dispLk
doneLk	
    retlw	    0
    
getString3
    movlw	LOW string3
    banksel	stringIndex1
    addwf	stringIndex1, w
    btfsc	STATUS, C
    incf	PCLATH, f
    movwf	PCL
string3	dt	"(LEAK!)", 0
;*******************************************************************************
	
;*********************Display "Reset" message***********************************
PrintReset
    banksel	stringIndex1	    ;Clear out string index counter
    clrf	stringIndex1	
    movlw	b'00000001'
    call	sendCommand	;Clear display reset cursor
    movlw	0x80
    call	sendCommand	;Set display address to beginning of 1st line
    
beginReset
    call	getResetString	;display "(Performing System Reset...)"
    ;finish if null
    iorlw	0
    btfsc	STATUS, Z	;end of string?
    goto	doneReset  	;if end of string quit looping
    ;Write character to display:
    call	sendData	;write char
    movlw	.1
    pagesel	delayMillis
    call	delayMillis	;delay
    pagesel$
    banksel	stringIndex1
    incf	stringIndex1, f	;increment string index to point to next char
    goto	beginReset
doneReset
    retlw	0
    
getResetString
    movlw	LOW resetString
    banksel	stringIndex1
    addwf	stringIndex1, w
    btfsc	STATUS, C
    incf	PCLATH, f
    movwf	PCL
resetString	dt	"Performing Reset...", 0    
;*******************************************************************************	
	
;-------------------------------------------------------------------------------
;***************Initialize LCD**************************************************
;-------------------------------------------------------------------------------
LCDInit
    ;25ms startup delay
    movlw	.50
    call	delayMillis

    movlw	b'00111000'	;user-defined "function set" command
    call	sendCommand
    
	;confirm entry mode set
    movlw	b'00000110'	;increment cursor, display shift off
    call	sendCommand
	
	;Display on, cursor on, blink on
    movlw	b'00001100'
		 ;-------1	;1=blink on
		 ;------1-	;1=cursor on
		 ;-----1--	;1=display on
    call	sendCommand
    ;LCD is now active and will display any data printed to DDRAM
    movlw	b'00000001'
    call	sendCommand	;Clear display reset cursor
    
    movlw	0x80
    call	sendCommand	;Set display address to beginning of 1st line
    
    retlw	0   
;-------------------------------------------------------------------------------
	
;-------------------------------------------------------------------------------
;***************Display connection info message*********************************
;-------------------------------------------------------------------------------

ESCinitializing  
   ;Set display address to beginning of 1st line
	movlw	0x80
	call	sendCommand
	;Print single characters
	movlw	b'01001001' ;I
	call	sendData
	movlw	b'01001110' ;N
	call	sendData
	movlw	b'01001001' ;I
	call	sendData
	movlw	b'01010100' ;T
	call	sendData
	movlw	b'01001001' ;I
	call	sendData
	movlw	b'01000001' ;A
	call	sendData
	movlw	b'01001100' ;L
	call	sendData
	movlw	b'01001001' ;I
	call	sendData
	movlw	b'01011010' ;Z
	call	sendData
	movlw	b'01001001' ;I
	call	sendData
	movlw	b'01001110' ;N
	call	sendData
	movlw	b'01000111' ;G
	call	sendData
	movlw	b'00101110' ;.
	call	sendData
	movlw	b'00101110' ;.
	call	sendData
	movlw	b'00101110' ;.
	call	sendData

	;Set display address to beginning of 4th line
	movlw	0xD4
	call	sendCommand
	;Print single characters
	movlw	b'01100010' ;b
	call	sendData
	movlw	b'01101001' ;i
	call	sendData
	movlw	b'01110100' ;t
	call	sendData
	movlw	b'01100100' ;d
	call	sendData
	movlw	b'01110010' ;r
	call	sendData
	movlw	b'01101001' ;i
	call	sendData
	movlw	b'01110110' ;v
	call	sendData
	movlw	b'01100101' ;e
	call	sendData
	movlw	b'01101110' ;n
	call	sendData
	movlw	b'01100011' ;c
	call	sendData
	movlw	b'01101001' ;i
	call	sendData
	movlw	b'01110010' ;r
	call	sendData
	movlw	b'01100011' ;c
	call	sendData
	movlw	b'01110101' ;u
	call	sendData
	movlw	b'01101001' ;i
	call	sendData
	movlw	b'01110100' ;t
	call	sendData
	movlw	b'01110011' ;s
	call	sendData

    retlw	0
    
;******************Display Various Gain Settings********************************
Disp25percent
    movlw	0x9B		;Move over one space from "Gain: "
    call	sendCommand
    movlw	b'00110010'	;2
    call	sendData
    movlw	b'00110101'	;5
    call	sendData
    movlw	b'00100101'	;%
    call	sendData
    movlw	b'00100000'	;blank space
    call	sendData
    retlw	0
    
Disp50percent
    movlw	0x9B		;Move over one space from "Gain: "
    call	sendCommand
    movlw	b'00110101'	;5
    call	sendData
    movlw	b'00110000'	;0
    call	sendData
    movlw	b'00100101'	;%
    call	sendData
    movlw	b'00100000'	;blank space
    call	sendData
    retlw	0
    
Disp75percent
    movlw	0x9B		;Move over one space from "Gain: "
    call	sendCommand
    movlw	b'00110111'	;7
    call	sendData
    movlw	b'00110101'	;5
    call	sendData
    movlw	b'00100101'	;%
    call	sendData
    movlw	b'00100000'	;blank space
    call	sendData
    retlw	0
    
Disp100percent
    movlw	0x9B		;Move over one space from "Gain: "
    call	sendCommand
    movlw	b'00110001'	;1
    call	sendData
    movlw	b'00110000'	;0
    call	sendData
    movlw	b'00110000'	;0
    call	sendData
    movlw	b'00100101'	;%
    call	sendData
    
    retlw	0

;***************************Display data***************************************
    ;Display Temp and Press Values
dispTemp
    ;Move to correct display address location
    movlw	0x87
    call	sendCommand
    
    movlw	.100
    banksel	Temperature
    subwf	Temperature, w
    btfss	STATUS, C	;C=0=neg number
    goto	sub100		;Temperature is less than 3 digits
    movlw	b'00110001'	
    call	sendData	;Temp>100 deg F, display "1"
sub100
    banksel	LCDdigit
    clrf	LCDdigit
tens
    incf	LCDdigit, f
    movlw	.10
    subwf	Temperature, f
    btfsc	STATUS, C	;C=0=neg number
    goto	tens
    decf	LCDdigit, f	;restore LCDdigit to pre-neg number value
    movlw	.10
    addwf	Temperature, f	;Restore Temperature to pre-neg number value
    movfw	LCDdigit
    pagesel	getDigit
    call	getDigit	;and get the corresponding tens value for LCD
    pagesel$
    call	sendData	;Then output it to the LCD
    banksel	LCDdigit
    clrf	LCDdigit	;Reset LCD digit to be output to LCD
ones
    incf	LCDdigit, f
    movlw	.1
    subwf	Temperature, f
    btfsc	STATUS, C	;C=0=neg number
    goto	ones
    decf	LCDdigit, f	;restore LCDdigit to pre-neg number value
    movlw	0x88
    call	sendCommand
    movfw	LCDdigit
    pagesel	getDigit
    call	getDigit	;and get the corresponding tens value for LCD
    pagesel$
    
    call	sendData	;Then output it to the LCD
    
    retlw	0
    
;Lookup table to retrieve digit for LCD display   
getDigit
    addwf   PCL, f
    retlw   b'00110000'	    ;0
    retlw   b'00110001'	    ;1
    retlw   b'00110010'	    ;2
    retlw   b'00110011'	    ;3
    retlw   b'00110100'	    ;4
    retlw   b'00110101'	    ;5
    retlw   b'00110110'	    ;6
    retlw   b'00110111'	    ;7
    retlw   b'00111000'	    ;8
    retlw   b'00111001'	    ;9    
    
    ;Display Temp and Press Headers
displayHeaders
    ;Clear display and reset cursor
    movlw	b'00000001'
    call	sendCommand
    ;Set display address to beginning of second line
    movlw	0x80
    call	sendCommand

    movlw	b'01010100'	;"T"
    call	sendData
    movlw	b'01100101'	;"e"
    call	sendData
    movlw	b'01101101'	;"m"
    call	sendData
    movlw	b'01110000'	;"p"
    call	sendData
    movlw	b'00111010'	;":"
    call	sendData
    ;Move over a few spaces and display deg F notation:
    movlw	0x89
    call	sendCommand
    movlw	b'11011111'	;"degree symbol"
    call	sendData
    movlw	b'01000110'	;"F"
    call	sendData
    movlw	0x8C
    call	sendCommand
    movlw	b'01111100'	;"|"
    call	sendData
    
;Set display address to beginning of second line
    movlw	0xC0
    call	sendCommand
    movlw	b'01010000'	;"P"
    call	sendData
    movlw	b'01110010'	;"r"
    call	sendData
    movlw	b'01100101'	;"e"
    call	sendData
    movlw	b'01110011'	;"s"
    call	sendData
    movlw	b'01110011'	;"s"
    call	sendData
    movlw	b'00111010'	;":"
    call	sendData
    movlw	0xCC
    call	sendCommand
    movlw	b'01111100'	;"|"
    call	sendData
    
;Set display address to beginning of third line
    movlw	0x94
    call	sendCommand
    movlw	b'01000111'	;"G"
    call	sendData
    movlw	b'01100001'	;"a"
    call	sendData
    movlw	b'01101001'	;"i"
    call	sendData
    movlw	b'01101110'	;"n"
    call	sendData
    movlw	b'00111010'	;":"
    call	sendData
    movlw	b'00100000'	;"blank space"
    call	sendData
    movlw	0xA0
    call	sendCommand
    movlw	b'01111100'	;"|"
    call	sendData
    
;Set display address to  4th line
    movlw	0xE0
    call	sendCommand
    movlw	b'01111100'	;"|"
    call	sendData
    
;Display battery voltage headers on right side of display
    movlw	0x8D
    call	sendCommand
    movlw	b'01000011'	;"C"
    call	sendData
    movlw	b'00110001'	;"1"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00101110'	;"."
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'01010110'	;"V"
    call	sendData
    
    movlw	0xCD
    call	sendCommand
    movlw	b'01000011'	;"C"
    call	sendData
    movlw	b'00110010'	;"2"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00101110'	;"."
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'01010110'	;"V"
    call	sendData
    
    movlw	0xA1
    call	sendCommand
    movlw	b'01000011'	;"C"
    call	sendData
    movlw	b'00110011'	;"3"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00101110'	;"."
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'01010110'	;"V"
    call	sendData
    
    movlw	0xE1
    call	sendCommand
    movlw	b'01000011'	;"C"
    call	sendData
    movlw	b'00110100'	;"4"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'00101110'	;"."
    call	sendData
    movlw	b'00100000'	;"space"
    call	sendData
    movlw	b'01010110'	;"V"
    call	sendData

    retlw	0
    
VoltageUpdate
    ;For variables cell1 cell2 cell3 and cell4, upper 4 bits represent the voltage
    ;value to the left of the decimal point (a "3" or "4") and the lower 4 bits 
    ;represent the voltage value to the right of the decimal point.
    
    ;-----------------------CELL 1----------------------------------------------
    ;Extract 4 lsb of cell1
    movlw	0x92		;Put LCD cursor in correct location
    call	sendCommand
    movlw	b'00001111'
    andwf	cell1, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    ;Extract 4 msb of cell 1 (will be a "3" or "4")
    movlw	0x90		;Put LCD cursor in correct location
    call	sendCommand
    banksel	cell1
    movfw	cell1
    movwf	cellShadow
    
    swapf	cellShadow, f
    movlw	b'00001111'
    andwf	cellShadow, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    
    ;-----------------------CELL 2----------------------------------------------
    ;Extract 4 lsb of cell2
    movlw	0xD2		;Put LCD cursor in correct location
    call	sendCommand
    movlw	b'00001111'
    andwf	cell2, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    ;Extract 4 msb of cell 1 (will be a "3" or "4")
    movlw	0xD0		;Put LCD cursor in correct location
    call	sendCommand
    banksel	cell2
    movfw	cell2
    movwf	cellShadow
    
    swapf	cellShadow, f
    movlw	b'00001111'
    andwf	cellShadow, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    
    ;-----------------------CELL 3----------------------------------------------
    ;Extract 4 lsb of cell3
    movlw	0xA6		;Put LCD cursor in correct location
    call	sendCommand
    movlw	b'00001111'
    andwf	cell3, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    ;Extract 4 msb of cell 1 (will be a "3" or "4")
    movlw	0xA4		;Put LCD cursor in correct location
    call	sendCommand
    banksel	cell3
    movfw	cell3
    movwf	cellShadow
    
    swapf	cellShadow, f
    movlw	b'00001111'
    andwf	cellShadow, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    
    ;-----------------------CELL 4----------------------------------------------
    ;Extract 4 lsb of cell4
    movlw	0xE6		;Put LCD cursor in correct location
    call	sendCommand
    movlw	b'00001111'
    andwf	cell4, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    ;Extract 4 msb of cell 1 (will be a "3" or "4")
    movlw	0xE4		;Put LCD cursor in correct location
    call	sendCommand
    banksel	cell4
    movfw	cell4
    movwf	cellShadow
    
    swapf	cellShadow, f
    movlw	b'00001111'
    andwf	cellShadow, w
    
    pagesel	getDigit
    call	getDigit	;Get the character code for LCD
    pagesel$
    call	sendData	;...and print it
    
    banksel	flags
    bcf		flags, 0	    ;Clear flag for LCD battery info refresh
   
    
    retlw	0    
   
    END
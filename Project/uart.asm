; UART transmit and receive subroutines as well as sendThrust soubroutine to 
;send data packets containing thruster speed/direction data
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    
    #define batteryCMD      (d'255')  ;Command from controller to perform battery read	
    #define tempCMD         (d'254')  ;Command from controller to perform temperature read
    #define leakCMD	    (d'253')  ;Command from controller to perfrom leak test
				      ;(done as part of normal datastream communication)
    #define ROVreset	    (b'10101010');Command to perform ROV reset (d'170')
    #define noROVreset	    (b'01010101');Command for no ROV reset (d'85')
    
    global      Transmit
    global	Receive
    global	sendThrust
    global	MS5837
    global	BatteryVoltage
    
    extern	transData
    extern	receiveData
    extern	state
    extern	forwardSpeed
    extern	reverseSpeed
    extern	upDownSpeed
    extern	lightsPWM
    extern	delayMillis
    extern	servoPosition
    extern	mul8
    extern	product8
    extern	mpcand8
    extern	div8
    extern	Q_dividend
    extern	divisor
    extern	lightsPWM
    extern	leadScrewPWM
    extern	gripTiltPWM
    extern	gripJawPWM
    extern	Leak
    extern	tempIntCtr
    extern	battIntCtr
    extern	Temperature
    extern	dispTemp
    extern	leakFlag
    extern	cell1
    extern	cell2
    extern	cell3
    extern	cell4
    extern	VoltageUpdate
    extern	PrintReset
    
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
;********************Transmit UART data packets*********************************
.uart code
 Transmit
    banksel	transData
    movfw	transData	
    banksel	TXREG
    movwf	TXREG		;data to be transmitted loaded into TXREG
				;and then automatically loaded into TSR
    nop
    nop
    banksel	PIR1
wait_trans
    btfss	PIR1, TXIF	;Is TX buffer full? (1=empty, 0=full)
    goto	wait_trans
    retlw	0

;*******************Receive UART data packets***********************************
 Receive
 
wait_receive
    banksel	PIR1
    btfss	PIR1, RCIF	;Is RX buffer full? (1=full, 0=notfull)
    goto	wait_receive	;wait until it is full
    banksel	RCSTA
    bcf		RCSTA, CREN
    banksel	RCREG
    movfw	RCREG		;Place data from RCREG into "receiveData"
    banksel	receiveData
    movwf	receiveData
    banksel	PIR1
    bcf	        PIR1, RCIF	    ;clear UART receive interrupt flag
    banksel	RCSTA
    bsf		RCSTA, CREN
    
    retlw	0

;**************Send thruster speed/direction data via UART**********************
;Send thruster data
    
sendThrust
    bcf		INTCON, IOCIE	;Disable joystick PB switch and gain adjustment
				;interrupts
    
    ;*******************TESTING************************************************
    ;goto	Z75
    ;*******************END TESTING*********************************************
    ;Determine if forwardSpeed/reverseSpeed are "stopped" values:
    movlw	.95
    banksel	forwardSpeed
    xorwf	forwardSpeed, w
    btfsc	STATUS, Z
    goto	XYdone		    ;forwardSpeed is stopped ("95") and by 
				    ;default so is "reverseSpeed"
;Process gain for xy-axis:
    banksel	PORTB
    btfsc	PORTB, 3	;25% gain?
    goto	XY25
    btfsc	PORTB, 4	;50% gain?
    goto	XY50
    btfsc	PORTB, 5	;75% gain?
    goto	XY75
    goto	GainDone	;100% gain is last remaining option
    
XY25	;*****************25% gain in xy-axis:*********************************
    ;ForwardSpeed--->newSpeed = 97 + gain*(oldSpeed-97):
    movlw	.97
    banksel	forwardSpeed
    subwf	forwardSpeed, w	;forwardSpeed = forwardSpeed - 97
    movwf	Q_dividend
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (forwardSpeed-97)/4
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	forwardSpeed	;forwardSpeed = (forwardSpeed-97)/4
    movlw	.97
    addwf	forwardSpeed, f	;forwardSpeed = 97 + (forwardSpeed-97) / 4
    ;ReverseSpeed--->newSpeed = 93 - gain*(93-oldSpeed)
    movlw	.93
    banksel	Q_dividend
    movwf	Q_dividend
    movfw	reverseSpeed
    subwf	Q_dividend, f	;Q_dividend = 93 - reverseSpeed
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (93-reverseSpeed)/4
    movlw	.93
    banksel	reverseSpeed
    movwf	reverseSpeed
    movfw	Q_dividend
    subwf	reverseSpeed, f	;reverseSpeed = 93 - Q_dividend
    goto	GainDone
    
XY50	;*****************50% gain in xy-axis:*********************************
    ;ForwardSpeed--->newSpeed = 97 + gain*(oldSpeed-97):
    movlw	.97
    banksel	forwardSpeed
    subwf	forwardSpeed, w	;forwardSpeed = forwardSpeed - 97
    movwf	Q_dividend
    movlw	.2
    movwf	divisor		;divisor=gain=1/2
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (forwardSpeed-97)/2
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	forwardSpeed	;forwardSpeed = (forwardSpeed-97)/2
    movlw	.97
    addwf	forwardSpeed, f	;forwardSpeed = 97 + (forwardSpeed-97) / 2
    ;ReverseSpeed--->newSpeed = 93 - gain*(93-oldSpeed)
    movlw	.93
    banksel	Q_dividend
    movwf	Q_dividend
    movfw	reverseSpeed
    subwf	Q_dividend, f	;Q_dividend = 93 - reverseSpeed
    movlw	.2
    movwf	divisor		;divisor=gain=1/2
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (93-reverseSpeed)/2
    movlw	.93
    banksel	reverseSpeed
    movwf	reverseSpeed
    movfw	Q_dividend
    subwf	reverseSpeed, f	;reverseSpeed = 93 - Q_dividend
    goto	GainDone
    
XY75	;*****************75% gain in xy-axis:*********************************
    ;ForwardSpeed--->newSpeed = 97 + gain*(oldSpeed-97):
    movlw	.97
    banksel	forwardSpeed
    subwf	forwardSpeed, w	;forwardSpeed = forwardSpeed - 97
    movwf	Q_dividend
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (forwardSpeed-97)/4
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	forwardSpeed	;forwardSpeed = (forwardSpeed-97)/4
    
    movwf	product8	;forwardSpeed is already in work reg
    movlw	.3
    movwf	mpcand8
    pagesel	mul8
    call	mul8
    pagesel$			;Result is held in product8
    
    movfw	product8
    movwf	forwardSpeed	;forwardSpeed = 3*(forwardSpeed-97) / 4
    movlw	.97
    addwf	forwardSpeed, f	;forwardSpeed = 97 + 3*(forwardSpeed-97) / 4
    ;ReverseSpeed--->newSpeed = 93 - gain*(93-oldSpeed)
    movlw	.93
    banksel	Q_dividend
    movwf	Q_dividend
    movfw	reverseSpeed
    subwf	Q_dividend, f	;Q_dividend = 93 - reverseSpeed
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (93-reverseSpeed)/4
    
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	product8
    movlw	.3
    movwf	mpcand8
    pagesel	mul8
    call	mul8
    pagesel$			;Result is held in product8
    
    movlw	.93
    banksel	reverseSpeed
    movwf	reverseSpeed
    movfw	product8
    subwf	reverseSpeed, f	;reverseSpeed = 93 - product8
    goto	GainDone
    
XYdone
    ;Determine if upDownSpeed is a "stopped" value:
    movlw	.95
    banksel	upDownSpeed
    xorwf	upDownSpeed, w
    btfsc	STATUS, Z
    goto	GainDone
    ;Process gain for z-axis:
    banksel	PORTB
    btfsc	PORTB, 3	;25% gain?
    goto	Z25
    btfsc	PORTB, 4	;50% gain?
    goto	Z50
    btfsc	PORTB, 5	;75% gain?
    goto	Z75
    goto	GainDone	;100% gain is last remaining option
Z25	;*****************25% gain in z-axis:*********************************
    ;Determine whether we are diving or surfacing:
    movlw	.95
    banksel	upDownSpeed
    subwf	upDownSpeed, w
    btfsc	STATUS, C	;C=0=neg number=diving
    goto	surface25	;positive # therefore surface 
  dive25	;Range of values: 70(fastest)-->93(slowest)
		;Similar to reverseSpeed gain adjustments.
    ;upDownSpeed--->newSpeed = 93 - gain*(93-oldSpeed)
    movlw	.93
    banksel	Q_dividend
    movwf	Q_dividend
    movfw	upDownSpeed
    subwf	Q_dividend, f	;Q_dividend = 93 - upDownSpeed
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (93-upDownSpeed)/4
    movlw	.93
    banksel	upDownSpeed
    movwf	upDownSpeed
    movfw	Q_dividend
    subwf	upDownSpeed, f	;upDownSpeed = 93 - Q_dividend
    goto	GainDone
  surface25	;Range of values: 97(slowest)-->120(fastest)
		;Similar to forwardSpeed gain adjustments
    ;upDownSpeed--->newSpeed = 97 + gain*(oldSpeed-97):
    movlw	.97
    banksel	upDownSpeed
    subwf	upDownSpeed, w	;upDownSpeed = upDownSpeed - 97
    movwf	Q_dividend
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (upDownSpeed-97)/4
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	upDownSpeed	;upDownSpeed = (upDownSpeed-97)/4
    movlw	.97
    addwf	upDownSpeed, f	;upDownSpeed = 97 + (upDownSpeed-97) / 4
    goto	GainDone
Z50	;*****************50% gain in z-axis:*********************************
    ;Determine whether we are diving or surfacing:
    movlw	.95
    banksel	upDownSpeed
    subwf	upDownSpeed, w
    btfsc	STATUS, C	;C=0=neg number=diving
    goto	surface50	;positive # therefore surface 
  dive50      ;Range of values: 70(fastest)-->93(slowest)
		;Similar to reverseSpeed gain adjustments.
    ;upDownSpeed--->newSpeed = 93 - gain*(93-oldSpeed)
    movlw	.93
    banksel	Q_dividend
    movwf	Q_dividend
    movfw	upDownSpeed
    subwf	Q_dividend, f	;Q_dividend = 93 - upDownSpeed
    movlw	.2
    movwf	divisor		;divisor=gain=1/2
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (93-upDownSpeed)/2
    movlw	.93
    banksel	upDownSpeed
    movwf	upDownSpeed
    movfw	Q_dividend
    subwf	upDownSpeed, f	;upDownSpeed = 93 - Q_dividend
    goto	GainDone
  surface50	;Range of values: 97(slowest)-->120(fastest)
		;Similar to forwardSpeed gain adjustments
    ;upDownSpeed--->newSpeed = 97 + gain*(oldSpeed-97):
    movlw	.97
    banksel	upDownSpeed
    subwf	upDownSpeed, w	;upDownSpeed = upDownSpeed - 97
    movwf	Q_dividend
    movlw	.2
    movwf	divisor		;divisor=gain=1/2
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (upDownSpeed-97)/2
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	upDownSpeed	;upDownSpeed = (upDownSpeed-97)/2
    movlw	.97
    addwf	upDownSpeed, f	;upDownSpeed = 97 + (upDownSpeed-97) / 2
    goto	GainDone
Z75	;*****************75% gain in z-axis:*********************************
    ;Determine whether we are diving or surfacing:
    movlw	.95
    banksel	upDownSpeed
    subwf	upDownSpeed, w
    btfsc	STATUS, C	;C=0=neg number=diving
    goto	surface75	;positive # therefore surface 
  dive75	;Range of values: 70(fastest)-->93(slowest)
		;Similar to reverseSpeed gain adjustments.
    ;upDownSpeed--->newSpeed = 93 - gain*(93-oldSpeed)
    movlw	.93
    banksel	Q_dividend
    movwf	Q_dividend
    movfw	upDownSpeed
    subwf	Q_dividend, f	;Q_dividend = 93 - upDownSpeed
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (93-upDownSpeed)/4
    
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	product8
    movlw	.3
    movwf	mpcand8
    pagesel	mul8
    call	mul8
    pagesel$			;Result is held in product8
    
    movlw	.93
    banksel	upDownSpeed
    movwf	upDownSpeed
    movfw	product8
    subwf	upDownSpeed, f	;upDownSpeed = 93 - product8
    goto	GainDone
  surface75	;Range of values: 97(slowest)-->120(fastest)
		;Similar to forwardSpeed gain adjustments
    ;upDownSpeed--->newSpeed = 97 + gain*(oldSpeed-97):
    
    movlw	.97
    banksel	upDownSpeed
    subwf	upDownSpeed, w	;forwardSpeed = upDownSpeed - 97
    movwf	Q_dividend
    movlw	.4
    movwf	divisor		;divisor=gain=1/4
    pagesel	div8
    call	div8
    pagesel$			;Q_dividend = (upDownSpeed-97)/4
    banksel	Q_dividend
    movfw	Q_dividend
    movwf	upDownSpeed	;upDownSpeed = (upDownSpeed-97)/4
    
    movwf	product8	;upDownSpeed is already in work reg
    movlw	.3
    movwf	mpcand8
    pagesel	mul8
    call	mul8
    pagesel$			;Result is held in product8
    
    movfw	product8
    movwf	upDownSpeed	;upDownSpeed = 3*(upDownSpeed-97) / 4
    movlw	.97
    addwf	upDownSpeed, f	;upDownSpeed = 97 + 3*(upDownSpeed-97) / 4
    goto	GainDone
GainDone

;********************Send the data stream to the ROV****************************    
;1) ROV directional "state"
    movlw	.1
    banksel	tempIntCtr
    addwf	tempIntCtr, f	;Increment the counter used to perform a temperature
				;reading
    banksel	state
    movfw	state
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the state value
WaitState    
    call	Receive
    banksel	state
    movfw	state
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitState
    
;2) Forward direction thruster speed
    banksel	forwardSpeed
    movfw	forwardSpeed
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the fwd speed value
WaitFwdSpeed    
    call	Receive
    banksel	forwardSpeed
    movfw	forwardSpeed
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitFwdSpeed
    
;3) Reverse direction thruster speed
    banksel	reverseSpeed
    movfw	reverseSpeed
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the rev speed value
WaitRevSpeed    
    call	Receive
    banksel	reverseSpeed
    movfw	reverseSpeed
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitRevSpeed
    
;4) Vertical direction thruster speed (Up/Down)
    banksel	upDownSpeed
    movfw	upDownSpeed
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the Up/Down speed value
WaitVertSpeed    
    call	Receive
    banksel	upDownSpeed
    movfw	upDownSpeed
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitVertSpeed    
    
;5) Camera servo tilt
    banksel	servoPosition
    movfw	servoPosition
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the camera servo value
WaitCamTilt   
    call	Receive
    banksel	servoPosition
    movfw	servoPosition
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitCamTilt    
    
;6) ROV LED lights PWM value
    banksel	lightsPWM
    movfw	lightsPWM
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the camera servo value
WaitROVleds   
    call	Receive
    banksel	lightsPWM
    movfw	lightsPWM
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitROVleds    
   
;7) ROV Lead Screw PWM value   
    banksel	leadScrewPWM
    movfw	leadScrewPWM
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the lead screw value
WaitLeadScrew    
    call	Receive
    banksel	leadScrewPWM
    movfw	leadScrewPWM
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitLeadScrew
   
;8) ROV Gripper tilt PWM value    
    banksel	gripTiltPWM
    movfw	gripTiltPWM
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the gripper tilt value
WaitTilt
    call	Receive
    banksel	gripTiltPWM
    movfw	gripTiltPWM
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitTilt
    
;9) ROV gripper jaw PWM value    
    banksel	gripJawPWM
    movfw	gripJawPWM
    movwf	transData
    call	Transmit    
    ;Wait for ROV to reply with the gripper jaw value
WaitJaw
    call	Receive
    banksel	gripJawPWM
    movfw	gripJawPWM
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitJaw
  
;10) Check for leak    
    movlw	leakCMD
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with signal for no leak (1) or leak (2)
WaitLeak    
    call	Receive
    movlw	.1
    xorwf	receiveData, w
    btfsc	STATUS, Z
    goto	CheckReset
    
    movlw	.2
    xorwf	receiveData, w
    btfsc	STATUS, Z
    goto	Wet
    goto	WaitLeak
Wet
    banksel	leakFlag
    btfsc	leakFlag, 0	;See if LCD has already had "leak"printed to it.
    goto	CheckReset
    pagesel	Leak
    call	Leak
    pagesel$
    banksel	leakFlag
    bsf		leakFlag, 0	;Set flag so we don't waste time updating the
				;LCD needlessly every time
    
;11) Check for ROV reset.    
CheckReset    
    banksel	PORTC
    btfsc	PORTC, 5	;Reset button is normally closed (high signal)
    goto	NoReset
YesReset
    pagesel	PrintReset
    call	PrintReset
    pagesel$			;Display system reset message
    
    movlw	ROVreset
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with reset command (.170)
WaitYes
    call	Receive
    movlw	ROVreset
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitYes
    ;Reset the controller\
    RESET
    ;goto	DoneStreaming
    
NoReset    
    movlw	noROVreset
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with No-reset command (.85)
WaitNo  
    call	Receive
    movlw	noROVreset
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitNo
    goto	DoneStreaming
    
DoneStreaming    
    bsf		INTCON, IOCIE	;Enable joystick PB switch and gain adjustment
				;interrupts
    retlw	0
    
;*****************Temperature related communications****************************
MS5837    
    bcf		INTCON, IOCIE	;Disable PORTB interrupts
    banksel	tempIntCtr
    clrf	tempIntCtr	;Reset temperature interval counter
    incf	battIntCtr, f	;Increment the battery interval counter	
    movlw	tempCMD
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the temperature command
WaitTempCMD    
    call	Receive
    movlw	tempCMD
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitTempCMD
    ;Receive the temperature value
ReceiveTemp    
    call	Receive
    movlw	tempCMD
    xorwf	receiveData, w	;Make sure we aren't receiving the temp command
    btfsc	STATUS, Z
    goto	ReceiveTemp
    ;See if temperature has changed from last reading
    banksel	Temperature
    movfw	Temperature
    xorwf	receiveData, w
    btfsc	STATUS, Z
    retlw	0		;Temperature hasn't changed so don't bother
				;printing it
SaveAndPrint
    movfw	receiveData				
    banksel	Temperature
    movwf	Temperature	;Save temperature value
    pagesel	dispTemp
    call	dispTemp	;Output temperature reading to LCD
    pagesel$
    bsf		INTCON, IOCIE	    ;Enable PORTB interrupts
    
    retlw	0
    
;****************************Battery related communications*********************
BatteryVoltage
    bcf		INTCON, IOCIE	;Disable PORTB interrupts
    banksel	battIntCtr
    clrf	battIntCtr	;Reset battery interval counter
    movlw	batteryCMD
    movwf	transData
    call	Transmit
    ;Wait for ROV to reply with the battery command
WaitBattCMD    
    call	Receive
    movlw	batteryCMD
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	WaitBattCMD    
waitCell1
    pagesel	Receive
    call	Receive
    pagesel$
    
    movlw	batteryCMD
    banksel	receiveData
    xorwf	receiveData, w	;Make sure packet isn't code the battery command
    btfsc	STATUS, Z
    goto	waitCell1
    banksel	receiveData
    movfw	receiveData
    banksel	cell1
    movwf	cell1
    ;Respond with decimal value "1"
    movlw	.1
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    

waitCell2
    banksel	PIR1
    btfss	PIR1, RCIF
    goto	waitCell2	;Wait for cell2 data to be sent from ROV    
    pagesel	Receive
    call	Receive
    pagesel$
    banksel	receiveData
    movfw	receiveData
    banksel	cell2
    movwf	cell2
    ;Respond with decimal value "2"
    movlw	.2
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
waitCell3
    banksel	PIR1
    btfss	PIR1, RCIF
    goto	waitCell3	;Wait for cell3 data to be sent from ROV    
    pagesel	Receive
    call	Receive
    pagesel$
    banksel	receiveData
    movfw	receiveData
    banksel	cell3
    movwf	cell3
    ;Respond with decimal value "3"
    movlw	.3
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$    
    
waitCell4
    banksel	PIR1
    btfss	PIR1, RCIF
    goto	waitCell4	;Wait for cell4 data to be sent from ROV    
    pagesel	Receive
    call	Receive
    pagesel$
    banksel	receiveData
    movfw	receiveData
    banksel	cell4
    movwf	cell4
    ;Respond with decimal value "4"
    movlw	.4
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$     
    
    ;Update the LCD
    pagesel	VoltageUpdate
    call	VoltageUpdate
    pagesel$
    
    bsf		INTCON, IOCIE	    ;Enable PORTB interrupts
    
    retlw	0    
   
    
    END



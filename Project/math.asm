;Math routines for control box
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    global	mul8
    global	div8
    
    extern	forwardSpeed	;forward speed variable
    extern	reverseSpeed	;reverse speed variable
    extern	upDownSpeed	;depth control variable
    extern	loopCount
    extern	remainder
    extern	divisor
    extern	Q_dividend
    extern	product8
    extern	mpcand8
    
    
    #define BANK0  (h'000')
    #define BANK1  (h'080')
    #define BANK2  (h'100')
    #define BANK3  (h'180')
    
.math code
 
 ;************************Multiply two 8-bit numbers****************************
 mul8
    movlw   .8
    banksel loopCount
    movwf   loopCount	;Set loopCount for 8 bit multiplication
    clrf    product8+1	;Zero out 2nd byte of product 8
    clrf    product8+2	;Zero out 3rd byte of product register. This will hold 
			;overflow from results of addition of mpcand8 to byte #2
			;over product8
 ;At beginning of routine,  product8 contains following:
;[-----upper byte------][-------lower byte----------]
;[---------zero-----------][------8 bit multiplier-------]
;		           [bit 0 of multiplier is control mechanism]
;Final Result is held in product8, product8+1 at end of mul routine
 testLsb
;(1) Test Lsb of multiplier (also lsb of product8)
    banksel product8
    btfss   product8, 0
    goto    mulShift	    ;lsb=0 so proceed to shift
addMpcand		    ;lsb=1 so add mpcand8 to left 1/2 of product8    
    banksel mpcand8
    movfw   mpcand8
    addwf   product8+1, f  ;Add mpcand8 to byte #1 of product8
    btfsc   STATUS, C	    ;Carry from this addition?
    incf    product8+2, f    ;If so increment 3rd byte of product8
;(2) Shift both two bytes the product8 register right one bit    
mulShift
    ;1st byte
    bcf	    STATUS, C	    ;Clear carry
    banksel product8
    rrf	    product8, f    ;right shift byte #0
    ;2nd byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product8+1, f  ;right shift byte #1
    btfsc   STATUS, C	    ;Carry due to right shift of byte #1?
    bsf	    product8, 7    ;Yes, so shift that bit into byte #0
    ;3rd byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product8+2, f  ;right shift byte #1
    btfsc   STATUS, C	    ;Carry due to right shift of byte #1?
    bsf	    product8+1, 7    ;Yes, so shift that bit into byte #1
    
    ;(3) Decrement loop counter
    decfsz  loopCount, f
    goto    testLsb	    ;reloop 8 times
 
    retlw	0
 ;********************End 8-bit multiplication routine**************************
 
 ;------------------------------------------------------------------------------
 
 ;**********************Divide two 8-bit numbers********************************
 div8
; At beginning of routine:
;	remainder = 0 at beginning
;	remainder at end of routine = 9 bits in length
;	loopCount = 8
;	divisor = 8 bits in length
;	Q_dividend = dividend, holds quotient at end of routine
    ;Zero out remainder
    banksel	remainder
    clrf	remainder
    movlw	.8
    movwf	loopCount	;set loop counter for 8-bit division
divShift
    ;Left-shift Q and remainder together (Q is shifted into remainder)
    bcf		STATUS, C	    ;Clear carry
    banksel	remainder
    rlf		remainder, f	    ;left-shift remainder
    btfsc	Q_dividend, 7	    ;msb of Q_dividend=1?
    bsf		remainder, 0	    ;yes so shift it into remainder
    
    rlf		Q_dividend, f	    ;left-shift	Q_dividend
    
    ;remainder = remainder-divisor
    movfw	divisor
    subwf	remainder, f
    ;check for borrow and restore is so
    btfss	STATUS, C	;C=0=neg number
    goto	resto
    ;no borrow so set lsb of Q_dividend to 1
    bsf		Q_dividend, 0
    decfsz	loopCount, f	;Decrement loop counter
    goto	divShift	;Reloop
    goto	divComplete	;Done so exit routine
resto
    banksel	Q_dividend
    bcf		Q_dividend, 0	;clear lsb of quotient
    ;restore remainder (A = remainder + divisor)
    movfw	divisor
    addwf	remainder, f
    decfsz	loopCount, f	;Decrement loop counter
    goto	divShift	;Reloop
divComplete	;Final result is held in Q_dividend
    retlw	0
;**********************End 8-bit division routine*******************************

    END

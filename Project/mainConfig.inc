    ;config file for main.asm
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    extern	Leak		;routine to set Leak warning light
    extern	Transmit	;UART transmit subroutine
    extern	Receive		;UART receive subroutine
    extern	sendThrust	;routine to send thruster data via UART
    extern	getAn0Disp	;displacement from "dead-center" for AN0
    extern	getAn1Disp	;displacement from "dead-center" for AN1
    extern	getMotorSpeed	;routine to determine correct speed of motor
    extern	checkSlop
    extern	peripheralInit
    extern	motors
    extern	sendData
    extern      sendCommand
    extern	displayHeaders
    extern	Disp25percent
    extern	Disp50percent
    extern	Disp75percent
    extern	Disp100percent
    extern	GainAdjust
    extern	mul8
    extern	div8
    extern	VoltageUpdate
    extern	ESCinitializing
    extern	MS5837
    extern	BatteryVoltage
    
    
    global	transData	;UART transmit variable
    global	receiveData	;UART receive variable
    global	state		;desired directional "state" of ROV
    global	forwardSpeed	;forward speed variable
    global	reverseSpeed	;reverse speed variable
    global	upDownSpeed	;depth control variable
    global	ADRESH0		;copy of ADRESH for AN0
    global	ADRESH1		;copy of ADRESH for AN1
    global	ADRESH4
    global	ADRESHc
    global	AN0disp
    global	AN1disp
    global	adcCounter
    global	compCounter
    global	positionSpeed
    global	motorTemp
    global	slopFlag
    global	userMillis
    global	dly16Ctr
    global	stringIndex1
    global	Temperature
    global	LCDdigit
    global	ctr
    global	servoPosition
    global	divisor
    global	Q_dividend
    global	remainder
    global	product8
    global	mpcand8
    global	loopCount
    global	delayMillis
    global	Delay16Us
    global	cell1
    global	cell2
    global	cell3
    global	cell4
    global	flags
    global	cellShadow
    global	lightsPWM
    global	leadScrewPWM
    global	gripTiltPWM
    global	gripJawPWM
      
    global	gripperFlags   
    global	ADCctr
    global	tempIntCtr
    global	battIntCtr
    global	leakFlag

    #define BANK0  (h'000')
    #define BANK1  (h'080')
    #define BANK2  (h'100')
    #define BANK3  (h'180')
    #define screwStop	        (d'94')	    ;PWM stop signal for lead screw	
    #define tempInterval        (d'40')	    ;Sets interval in between temperature
					    ;readings. Based on the length of time 
					    ;it takes to complete a single 
					    ;data stream sequence. (about 50mS)
					    
    #define battInterval	(d'5')	    ;Sets interval in between battery
					    ;voltage readings. It is incremented
					    ;after the reception of temperature data.
					    ;(about once every 10 seconds)
				

    ; CONFIG1
; __config 0x9C2
    __CONFIG _CONFIG1, _FOSC_HS & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF
; CONFIG2
; __config 0xDDFF
    __CONFIG _CONFIG2, _WRT_OFF & _VCAPEN_OFF & _PLLEN_ON & _STVREN_OFF & _BORV_LO & _LVP_OFF
    
    ;Variables accessible in all banks:
    MULTIBANK	    UDATA_SHR
    userMillis	    RES	1
    adcCounter	    RES	1	;counter to be increented till value in
				;ADRESH is reached
    ADRESHc	    RES	1	;copy of ADRESH
    ADRESH0	    RES	1	;copy of value from pin AN0
    ADRESH1	    RES	1	;copy of value from pin AN1
    ADRESH4	    RES	1	;copy of value from pin AN2
    AN0disp	    RES	1	;displacement of ADRESHO from 127
    AN1disp	    RES	1	;displacement of ADRESH1 from 127
    transData	    RES	1	;Data to be transmitted via UART
    receiveData	    RES	1	;Data received via UART

    ;General Variables
    GENVAR1	    UDATA
    w_copy	    RES 1	;variable used for context saving (work reg)
    status_copy	    RES 1	;variable used for context saving (status reg)
    pclath_copy	    RES 1	;variable used for context saving (pclath copy)
    positionSpeed   RES	1	;value returned from getMotorSpeed routine
				;to be placed in forward, reverse and upDown speeds
    forwardSpeed    RES	1	;Forward value for CCPR1L
    reverseSpeed    RES	1	;Reverse value for CCPR2L
    upDownSpeed	    RES	1	;CCPR3L value for up/down thrusters
    slopFlag	    RES	1
    dly16Ctr	    RES	1
    motorTemp	    RES	1
    compCounter	    RES	1	;counter to be incremented once every 6 servo
				;steps to give full range of motion
    state	    RES	1	;desired directional "state" of ROV
    stringIndex1    RES	1	;String for LCD
    Temperature	    RES	1	;Temperature value (in Farenheit) received from ROV via UART
    LCDdigit	    RES	1	;Used for converting binary value to decimal value for LCD
    ctr		    RES	1	;ctr and servoPosition
    servoPosition   RES	1       ;are used for camera tilt
   ;8-bit multiplication:
    product8	    RES	3   ;24 variable for 8-bit mul routine
    mpcand8	    RES	1   ;8-bit multiplicand for 8-bit mul routine
    ;8-bit division:
    remainder	    RES	1   ;9-bit number required for remainder (remainder 
			    ;needs to be zeroed out at beginning of each call to div8)
    divisor	    RES	1
    Q_dividend	    RES	1   ;dividend/quotient
    loopCount	    RES	1   ;counter mul/div routines
    ;Battery readings	
    cell1	    RES	1   ;UART packet containing encoded cel1 1 voltage value
    cell2	    RES	1   ;UART packet containing encoded cell 2 voltage value
    cell3	    RES	1   ;UART packet containing encoded cell 3 voltage value
    cell4	    RES	1   ;UART packet containing encoded cell 4 voltage value	
    flags	    RES	1   ;Various flags
    cellShadow	    RES	1   ;Shadow register to manipulate cell voltage values 
			    ;for LCD display
    lightsPWM	    RES	1   ;Value to be sent to PWM module of lights controller
    ;Gripper Arm
    ADCctr	    RES	1
    leadScrewPWM    RES	1	;PWM value to be sent to lead screw motor
    gripTiltPWM	    RES	1	;PWM value to be sent to gripper tilt servo
    gripJawPWM	    RES	1	;PWM value to be sent to gripper claw servo
      
    gripperFlags    RES	1	;Used for motion control of gripper arm
    
    
    tempIntCtr      RES	1	;Counter used to set interval for performing temperature
				;readings.
    battIntCtr	    RES	1	;Counter used to set interval for performing battery
				;voltage readings
    leakFlag	    RES	1	;leakFlag, 0 = 1 if LCD has already been updated
				;to alert the user of a leak

   
    ;*********************INITILIZE PERIPHERALS*********************************
    
    list	    p=16f1937	   ;list directive to define processor
    #include    <p16f1937.inc>	   ;processor specific variable definitions
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    global	peripheralInit
    extern	delayMillis
    extern	motorTemp
    extern	transData
    extern	forwardSpeed
    extern	reverseSpeed
    extern	upDownSpeed
    extern	receiveData
    extern	slopFlag
    extern	LCDInit
    extern	cell1
    extern	cell2
    extern	cell3
    extern	cell4
    extern	cellShadow
    extern	flags
    extern	lightsPWM
    extern	leadScrewPWM
    extern	gripTiltPWM
    extern	gripJawPWM
    
    extern	gripperFlags   
    extern	ADCctr
    extern	state
    extern	tempIntCtr
    extern	battIntCtr
    extern	leakFlag
    extern	Temperature
    
    #define BANK0  (h'000')
    #define BANK1  (h'080')
    #define BANK2  (h'100')
    #define BANK3  (h'180')
    #define screwStop	       (d'94')	    ;PWM stop signal for lead screw	
    #define gripperTiltNeutral (d'88')
    
.initialization code
peripheralInit
    banksel BANK1
    ;************************Configure PORTS************************************
    movlw   b'11111111'		
	     ;--1-----		;PORTA, 5 = Camera tilt ADC
	     ;----1---		;PORTA, 3 = Lead Screw ADC
	     ;-----1--		;PORTA, 2 = ROV lights ADC
	     ;------1-		;PORTA, 1 = Joystick left/right ADC
	     ;-------1		;PORTA, 0 = Joystick fwd/rev ADC
    movwf   (TRISA ^ BANK1)	
    movlw   b'11111001'		    
	     ;1-------		PORTB, 7 = ICSPDAT
	     ;-1------		PORTB, 6 = ICSPCLK
	     ;--1-----		PORTB, 5 = Position 3 for rot switch (75% gain)
	     ;---1----		PORTB, 4 = Position 2 for rot switch (50% gain)
	     ;----1---		PORTB, 3 = Position 1 for rot switch (25% gain)
	     ;-----0--		PORTB, 2 = RS for LCD 
	     ;------0-		PORTB, 1 = E for LCD
	     ;-------1		PORTB, 0 = interrupt signal from joystick PB switch
    movwf   (TRISB ^ BANK1)
    
    movlw   b'11111000'		;PORTC, 7 = RX pin for UART, PORTC, 2=P1A (PWM)         
    movwf   (TRISC ^ BANK1)
    
    movlw   b'00000000'		;Leak and ready lights
    movwf   (TRISD ^ BANK1)
    
    movlw   b'00000011'
	     ;------1-		;PORTE, 1 = gripper claw ADC
	     ;-------1		;PORTE, 0 = gripper tilt ADC
    movwf   (TRISE ^ BANK1)
    
     ;************************Config ADC:****************************************
    movlw	b'00011111' 
		 ;-------1	;AN0=Fwd/Rev
		 ;------1-	;AN1=left/right
		 ;-----1--	;AN2=ROV lights
		 ;----1---	;AN3=lead screw
		 ;---1----	;AN4=Camera tilt
    banksel	ANSELA	    
    movwf	ANSELA
    
    movlw	b'00000011' 
		 ;------1-		;RE1, AN6 = gripper claw
		 ;-------1		;RE0, AN5=gripper tilt
    banksel	ANSELE	    
    movwf	ANSELE
    
    movlw	b'00010000'
		;0-------  ADFM=0 (left justified. 8MSBs are in ADRESH
		;-001----  ADCS<0:2>, bits 4-6 =001. (2.0uS)
			;FOSC/8=4Mhz/8 (doubles instruction cycle time)
			;Instruction Cycle period (TCY) now equals
			;2uS (greater than 1.6uS necessary for ADC)
    banksel	ADCON1
    movwf	ADCON1
    
    banksel	ANSELB
    clrf	ANSELB
    clrf	ANSELD
    
    banksel	PORTD
    clrf	PORTD
    clrf	PORTC
    clrf	PORTB
    
    
    ;************************Configure timer0************************************
    ;With 4Mhz external crystal, FOSC is not divided by 4.
    ;Therefore each instruction is 1/4 of a microsecond (250*10^-9 sec.)
    movlw	b'11000110'	
		 ;1-------	WPUEN=0, all weak pull-ups are disabled
		 ;-1------	INTEDG=1, Interrupt on rising edge of INT pin
		 ;--0-----	TMR0CS=0, TMR0 clock source=internal instruction
			        ;	  (FOSC/4)
		 ;---0----	;TMR0SE=0, disregard
		 ;----0---	;PSA=0, prescaler assigned to TMR0 module
		 ;-----110	;PS<2:0> = 110, TMRO increments once every 128
				;instruction cycles 
				 ;TMR0 increments once every 128 * (250*10^-9) sec
				;or once every 32uS
    banksel	OPTION_REG	
    movwf	OPTION_REG	
	
    ;4Mhz external crystal:
    movlw	b'00000000'
    banksel	OSCCON
    movwf	OSCCON
    
    ;***********************Configure PWM***************************************
    movlw       b'00000110'     ; configure Timer2:
                ; -----1--          turn Timer2 on (TMR2ON = 1)
                ; ------10          prescale = 16 (T2CKPS = 10)
    banksel     T2CON           ; -> TMR2 increments every 4 us
    movwf       T2CON
    movlw       .140            ; PR2 = 140
    banksel     PR2             ; -> period = 560uS
    movwf       PR2             ; -> PWM frequency = 1.8 kHz
    ;Configure CCP1, CCP2 and CCP3 to be based off of TMR2:
    banksel     CCPTMRS0
    movlw	b'11111100'
		 ;------00	;CCP1 based off of TMR2
    ;configure CCP1
    movlw       b'00001100'     ; configure CCP:
                ; 00------          single output (P1M = 00 -> CCP1 active)
                ; --00----          DC1B = 00 -> LSBs of PWM duty cycle = 00
                ; ----1100          PWM mode: all active-high (CCP1M = 1100)
    banksel     CCP1CON         ; -> single output (CCP1) mode, active-high
    movwf       CCP1CON
    banksel	CCPR1L
    clrf	CCPR1L
    ;***************************************************************************
    
    clrf	motorTemp
    
    ;*********************************CONFIGURE UART********************************
    ;Configure Baud rate
    movlw	b'11111111' ;=58
    banksel	SPBRG
    movwf	SPBRG	    
    
    ;movlw	b'00000011' ;=3 Total value of SPBRG/n = 826
    ;banksel	SPBRGH
    ;movwf	SPBRGH
    
    banksel	BAUDCON
    movlw	b'00000000'
		 ;----1---	BRG16 (16 bit baud rate generator)
    movwf	BAUDCON
    
    ;Enable Transmission:
    movlw	b'00100100'
		 ;-0------  :8-bit transmission (TX9 = 0)
		 ;--1-----  :Enable transmission (TXEN = 1)
		 ;---0----  :Asynchronous mode (SYNC = 0)
		 ;-----0--  :Low speed baud rate (BRGH = 0)
    banksel	TXSTA
    movwf	TXSTA
		 
    ;Enable Reception:
    movlw	b'10010000'
		 ;1-------  :Serial port enabled (SPEN = 1)
		 ;-0------  :8-bit reception (RX9 = 0)
		 ;---1----  :Enable receiver (CREN = 1)
		 ;----0---  :Disable address detection (ADDEN = 0)
    ;			     all bytes are received and 9th bit can be used as
    ;			     parity bit
    banksel	RCSTA
    movwf	RCSTA
    
    ;1/4 second delay
    movlw	.10
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    movlw	.7
    banksel	state
    movwf	state
    movlw	.95
    movwf	forwardSpeed
    movwf	reverseSpeed
    movwf	upDownSpeed
    
    movlw	.34
    movwf	lightsPWM	;ROV lights in default state of off
    
    movlw	.250
    banksel	motorTemp
    movwf	motorTemp
    clrf	receiveData
    clrf	transData
    clrf	slopFlag
    
    banksel	cell1
    clrf	cell1
    clrf	cell2
    clrf	cell3
    clrf	cell4
    clrf	cellShadow
    clrf	flags
    
    movlw	screwStop
    banksel	leadScrewPWM
    movwf	leadScrewPWM
    movwf	gripTiltPWM
    movwf	gripJawPWM
    
    movlw	b'00001010'
    movwf	gripperFlags	;Load a gripper state of neutral position for
				;tilt and jaws position
    
    
    ;*************************Enable interrupts*********************************
    movlw	b'11000000'
	         ;1-------	;Enable global interrupts (GIE=1)
		     ;-1------	;Enable peripheral interrupts (PEIE=1)
		     ;--0-----	;Disable TMR0 interrupts (TMROIE=0)
		     ;---0----	;Disable RBO/INT external interrupt (INTE=0)
		     ;----0---	;Disable interrupt on change for PORTB (IOCIE=0)
				        ;PORTB IOC will be enabled after ESC init
    movwf	INTCON
    
    banksel	PIE1
    clrf	PIE1
    
    ;Enable interrupt on change for PORTB
    movlw	b'00111001'	
	         ;----1---	PORTB, 3 = Position 1 for rot switch (25% gain)
		     ;---1----	PORTB, 4 = Position 2 for rot switch (50% gain)
		     ;--1-----	PORTB, 5 = Position 3 for rot switch (75% gain)
		     ;-------1	PORTB, 0 = joystick pb-switch
    banksel	IOCBP
    movwf	IOCBP
    
    ;**************************Initialize LCD***********************************
    pagesel	LCDInit
    call	LCDInit
    pagesel$
    
    banksel	tempIntCtr
    clrf	tempIntCtr
    clrf	leakFlag
    clrf	Temperature
    clrf	battIntCtr
    
    
    
    retlw	   0


    END
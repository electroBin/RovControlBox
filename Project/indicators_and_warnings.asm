;Code for leak detector, ESC ready indicator light and Battery voltage
    
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    global      Leak
    
    
    extern	PrintLeak
    
    extern	Receive
    extern	receiveData
    extern	Transmit
    extern	transData
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
	
    
.indicator code 
;********************LEAK code*************************************************
Leak
    banksel	PORTC
    bsf		PORTC, 0
    movlw       .125              
    banksel	CCPR1L
    movwf       CCPR1L          ; -> PWM duty cycle = 18%
    pagesel	PrintLeak
    call	PrintLeak
    pagesel$
    
    retlw	0
    


    
    
    END


